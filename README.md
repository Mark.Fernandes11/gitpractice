GIT Scenarios and Workflows

1.) Create a new Repo or clone.
2.) Add a new file.
3.) Commit the changes.
4.) Push it to remote.
5.) Change the file in remote.
6.) Merge the local.
7.) Change the file in local and remote to create a conflict and resolve.
8.) Create Branch.
9.) Change the file in remote in master branch.
10.) Change the file in branch, check the difference and merge the change in the branch.
